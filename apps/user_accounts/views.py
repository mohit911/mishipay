from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponseRedirect

from django.urls import reverse
# Create your views here.


def home(request):
    return render(request, 'home.html')


def register(request):
    context = {}
    if request.method == 'POST':
        try:
            username = request.POST.get('email')
            password1 = request.POST.get('password1')
            password2 = request.POST.get('password2')

            if password2 == password1:
                User.objects.create_user(
                    password=password1,
                    username=username)

                context = {'success': True, 'message': 'Successfully saved. Please Login to continue'}
                return render(request, "register.html", context)

        except:
            context = {'success': False, 'message': 'NOT saved! Please try again later!'}
            return render(request, "register.html", context)

    return render(request, "register.html", context)


def login(request):
    template = "login.html"
    if request.method == 'POST':
        u = request.POST.get('username', '')
        p = request.POST.get('password', '')
        user = auth.authenticate(username=u, password=p)
        context = {}
        if request.user.is_authenticated:
            context = {'login': 'already LoggedIn'}
            context['username'] = request.user.username
            return render(request, template, context)
        else:
            if user is not None:
                if user.is_active:
                    auth.login(request, user)
                    return HttpResponseRedirect(reverse('home'))
                else:
                    context = {}
                    context['login'] = 'Account deactivated'
                    return render(request, template, context)

            else:
                context = {}
                context['login'] = 'Invalid Login'
                return render(request, template, context)

    else:
        if request.user:
            context = {"username": request.user.username}

        return render(request, template, context)


@login_required(login_url='login')
def logout(request):
    if request.user.is_authenticated:
        auth.logout(request)
        
        context = {}
        context['login'] = 'Logout sucessfull'
        return login(request)
    else:
        pass
